/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviorsocial;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.ceil;
import static java.lang.Math.round;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 *
 * @author Alfredo Valenzuela
 */
public class Universo {
    private static Agente universo[][];
    private boolean movimiento;
    final int MAXCAREL= 2;
    private  ArrayList<Agente>u= new ArrayList<>();
    private  ArrayList<Agente> encarcelados = new ArrayList<>();
    private  ArrayList<Agente> rebelados= new ArrayList<>();
    private ArrayList<Agente>policias= new ArrayList<>();
    private ArrayList<Agente> ciudadanos= new ArrayList<>();
    private  double nPolicias;
    private  double nCiudadanos;    
    private  double nAgentes;
    Random r1= new Random();
    private int dias;
    private int dimension;
    //constructor
   public Universo(Boolean movimiento,int dias, int dimension){
        this.movimiento = movimiento;
        this.dias = dias;
        this.dimension = dimension;
    }
    //gettter
    public double getNpolicias(){
        return nPolicias;
    }
    public  double getNagentes(){
        return nCiudadanos;
    }
    //metodos
    public void crearUniverso(int dimension,int densidadPoli, int densidadCiudad,double legitimidadGob1) throws InterruptedException{
        nPolicias=  Math.ceil(((dimension*dimension*densidadPoli)/100.0));//saca el numero de policias
        System.out.println("Numeros de policias: "+nPolicias);
        nCiudadanos= Math.ceil((dimension*dimension*densidadCiudad)/100.0);//saca el numero de los ciudadanos
        System.out.println("Números de ciudadanos: "+nCiudadanos);
        nAgentes= nPolicias+nCiudadanos;
        universo= new Agente[dimension][dimension];
       
        for(int x=0;x<nCiudadanos;x++){//AGREGO LOS CIUDADANOS AL UNIVERSO
            int columna=r1.nextInt(dimension);
            int fila=r1.nextInt(dimension);
            Ubicacion uC= new Ubicacion(fila,columna);
            if(universo[fila][columna]==null){
                Agente ciudadano= new Ciudadano(uC);
            universo[fila][columna]= ciudadano;
            u.add(ciudadano);
            ciudadanos.add(ciudadano);
            }else{
                x--;    
            }
        }
            for(int y=0;y<nPolicias;y++){//AGREGO A LOS POLICIAS AL UNIVERSO
            int columnaP=r1.nextInt(dimension);
            int filaP=r1.nextInt(dimension);
            Ubicacion uP= new Ubicacion(filaP,columnaP);
            if(universo[filaP][columnaP]== null){
                Agente policia= new Policia(uP);
                universo[filaP][columnaP]=policia;
                u.add(policia);
                policias.add(policia);
            }else{
                y--;
            }
        }
        imprimirUniverso(universo);
                           
}
    public  void crearArchivoParametros() throws IOException{
         LocalDateTime fecha= LocalDateTime.now();
         System.out.println("antes");
         String archivo = "./parametros_"+fecha.format(DateTimeFormatter.ofPattern("dMMMuuuuHHmmss"))+".txt";
        System.out.println("despues");
        File f= new File(archivo);
        try{
            String linea= "";
            FileWriter fw= new FileWriter(f);
            BufferedWriter bw= new BufferedWriter(fw);
            PrintWriter pw= new PrintWriter(bw);
            linea="Campo Vision: "+Integer.toString(BehaviorSocial.getCampoVision())+"\n"+"Densidad Ciudad: "+Integer.toString(BehaviorSocial.getDensidadCiudad())+"\n"+"Densidad Policia: "+Integer.toString(BehaviorSocial.getDensidadPoli())+"\n"+"Numero Dias: "+Integer.toString(BehaviorSocial.getDias())+
                    "\n"+"Dimension: "+Integer.toString(BehaviorSocial.getDimension());
            pw.write(linea);
            pw.close();
            bw.close();
        }
           catch(IOException e){
               
           }
    }
    public  void crearArchivoSimulacion(int dias){
        LocalDateTime fecha= LocalDateTime.now();
        String archivo = "simulacion_"+fecha.format(DateTimeFormatter.ofPattern("dMMMuuuuHHmmss"))+".txt";
        File f = new File(archivo);
       
            try{
                 for(int i=0;i<dias;i++){
            String linea= "";
            FileWriter fw= new FileWriter(f);
            BufferedWriter bw= new BufferedWriter(fw);
              PrintWriter pw= new PrintWriter(bw);
            linea="# de ciudadanos encarcelados: "+Integer.toString(encarcelados.size())+", # de ciudadanos rebelados:"+Integer.toString(rebelados.size())+", # de  cidudadanos tranquilos:"+Double.toString(nCiudadanos-rebelados.size()-encarcelados.size());
            pw.write(linea);
            pw.close();
            bw.close();
        }
            }
           catch(IOException e){
               
           }
        
    }
    public void imrpimirParametros(double legitimidadGobierno){
        
        for(Agente a :u)
            if(a instanceof Ciudadano){
                System.out.println("Parametro Ciudadano");
                System.out.println(legitimidadGobierno);
                 double agravio=((Ciudadano) a).calcularAgravio(legitimidadGobierno);
                 System.out.println("agravio:"+agravio);
                 double probabilidad=((Ciudadano) a).probabilidadRetencion(nPolicias,nCiudadanos);
                 System.out.println("Probabilidada de detencion: "+probabilidad);
                 double riesgo= ((Ciudadano) a).calcularRiesgoneto(nPolicias,nCiudadanos);
                 System.out.println("Riesgo neto: "+riesgo);
                 ((Ciudadano) a).seRebela(legitimidadGobierno,nPolicias,nCiudadanos);
                System.out.println("--- ------------");
                
            }
    }
    
    public  void ActualizarUniverso(double legitimidad1) throws InterruptedException{
           for(int i =0;i<dias;i++){
               crearArchivoSimulacion(dias);
            System.out.println("Nos encontramos en el dia "+(1+i));
            if(movimiento){
           
           Agente uNuevo[][]= new Agente[dimension][dimension];
           for(Agente a :u){
               if( a instanceof Policia){
               int columna= a.getLugar().getColumna();
               int fila=a.getLugar().getFila();
               uNuevo[fila][columna]=a;
           }
           }
          for(Agente a :u){
               if(a instanceof Ciudadano){
                   if(!((Ciudadano) a).isEncarcelado()){
                       boolean validar= true;
                        ((Ciudadano) a).seRebela(legitimidad1, nPolicias, nAgentes);
                        //System.out.println(rebelados.contains(a));
                        if(((Ciudadano) a).getRebelado()==true  && !rebelados.contains(a)){
                           rebelados.add(a);  
                           ciudadanos.remove(a);
                           encarcelados.remove(a);
                       }                
                             do{
                             int columna=r1.nextInt(dimension);
                             int fila=r1.nextInt(dimension);
                             Ubicacion uN= new Ubicacion(fila,columna);
                             if(a.getLugar()!=uN && uNuevo[fila][columna]== null){
                                 uNuevo[fila][columna]= a;
                                 validar= false;
                             }
                             
                             }
                                 while(validar);
                                     }
                   else{
                    if(((Ciudadano) a).getTurnosCarcel()<MAXCAREL){
                         ((Ciudadano) a).setTurnosCarcel(((Ciudadano) a).getTurnosCarcel()+1);
                    }
                    else{
                        ((Ciudadano) a).liberar();
                        ciudadanos.add(a);
                        encarcelados.remove(a);
                        rebelados.remove(a);
                        
                        System.out.println("Se libero un desgraciado!!!!");
                        boolean validar= true;
                        do{
                             int columna=r1.nextInt(dimension);
                             int fila=r1.nextInt(dimension);
                             Ubicacion uN= new Ubicacion(fila,columna);
                             if(a.getLugar()!=uN && uNuevo[fila][columna]== null){
                                 uNuevo[fila][columna]= a;
                                 validar= false;
                             }
                             
                             }
                                 while(validar);
                                     }
                     
               }
            }   
               
          }
          
           universo=uNuevo; 
            }
               int nencarcel = 0;
               for (Agente policia : policias) {
                   ArrayList<Agente> cercanos = rebeladosCercanos((Policia)policia, universo);
                   if (cercanos.size()>0){
                       int random= r1.nextInt(cercanos.size());
                       
                       int uFEncarcelado=cercanos.get(random).lugar.getFila();
                       int uCEncarcelado=cercanos.get(random).lugar.getColumna();
                       ((Ciudadano)cercanos.get(random)).setEncarcelado(true);
                       //System.out.println(cercanos.get(random));
                       encarcelados.add(cercanos.get(random));
                       ciudadanos.remove(cercanos.get(random));
                       rebelados.remove(cercanos.get(random));
                       universo[uFEncarcelado][uCEncarcelado] = null;
                       nencarcel++;
                   }
                   
               }
               System.out.println("n encarcelados: "+nencarcel);
            
           imprimirUniverso(universo);
    }
   
    }
    public   ArrayList<Agente> getRebelados() {
        return rebelados;
    }
    

public void imprimirUniverso( Agente universo[][]) throws InterruptedException{
    System.out.println(" Numero de rebeldes: "+rebelados.size()+" Numero de encarcelados: "+encarcelados.size()+" Numero tranquilos: "+ciudadanos.size());
     for(int x = 0; x < universo.length; x++){
    	for(int j = 0; j < universo[x].length; j++){
                    if (universo[x][j]==null){
                        System.out.print("0 ");
                   }
                    else{
                       System.out.print(universo[x][j]);
                            }
                   }
       
        System.out.println();
     }     
        Thread.sleep(1000);   
       System.out.println();
    }

public ArrayList<Agente> rebeladosCercanos(Policia poli, Agente universo[][]){
    int filaMax = poli.getLugar().getFila()+poli.getRango();
    int filaMin = poli.getLugar().getFila() - poli.getRango();
    int columnaMax = poli.getLugar().getColumna()+poli.getRango();
    int columnaMin = poli.getLugar().getColumna() - poli.getRango();
    
    if (filaMax > universo.length-1){
        filaMax = universo.length-1;
    }
    if (filaMin < 0){
        filaMin = 0;
    }
    if (columnaMax > universo.length-1){
        columnaMax = universo.length-1;
    }
    if (columnaMin < 0){
        columnaMin = 0;
    }
    ArrayList<Agente> cercanos = new ArrayList<>();
    for (int i = filaMin; i <= filaMax; i++) {
        for (int j = columnaMin; j <= columnaMax; j++) {
            if (universo[i][j] instanceof Ciudadano){
                if(((Ciudadano) universo[i][j]).getRebelado()==true){
                    cercanos.add(universo[i][j]);
                    
                }
           }
        }
    }
    return cercanos;
}   
}
