/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviorsocial;

import java.io.IOException;
import java.util.*;

/**
 *
 * @author Gilson Ponce Briones
 */
public class BehaviorSocial {
private static Scanner sc = new Scanner(System.in);
private static int dimension;
private  static int dias;
private  static  int densidadCiudad;
private static int densidadPoli;
private static int campoVision;
private static double legitimidadGob;
private static String movimiento;

    /**
     * @param args the command line arguments
     */
//getter
public double getLegitimidad(){return legitimidadGob;}
//metodos
    static public void presentacion() throws InterruptedException, IOException{
        boolean validar;
        System.out.println("/////////////////////////////////////////////////////////////////////");
        System.out.println("//    ********     **      ********      **********       ***      //");
        System.out.println("//    **         **   **   **     *      ***    ***       ***      //");
        System.out.println("//    *****        **      ********      ***    ***       ***      //");
        System.out.println("//    **        *    **    **            ***    ***       ***      //");
        System.out.println("//    ********     **      **            **********       ******** //");
        System.out.println("//-----------------------------------------------------------------//");
        System.out.println("//                PROYECTO DE COMPORTAMIENTO SOCIAL                //");
        System.out.println("//-----------------------------------------------------------------//");
        System.out.println("/////////////////////////////////////////////////////////////////////");
        System.out.println("");
        System.out.println(" Bienvenidos a este Universo");
        do{//valida que  sean menores o igual a 50
                System.out.print("Ingrese las dimensiones que tendra el universo de (nxn) n: ");
                dimension=sc.nextInt();
                if (dimension > 50){
                    System.out.println("Ingrese un numero menor o igual a 50");
                    validar = true;
                }else{
                     validar = false;
                }    
        }while(validar);
        do{//valida que no ingrese mayores de 100
                System.out.print("Cual sera el % de policias de 0-100%: ");
                densidadPoli= sc.nextInt();
                int diff = 100-densidadPoli;
                System.out.print("Cual sera el % de Ciudadanos de 0-"+diff+"%: ");
                densidadCiudad= sc.nextInt();
                if (densidadPoli >100 || densidadCiudad>100){
                    System.out.println("Rango de numero no permitido, intente otra vez");
                    validar = true;
                } else{
                     if(diff <= 0){
                         densidadCiudad = 0;
                         validar = false;
                     }else if (densidadCiudad <= diff){
                         validar = false;
                     }else{
                         System.out.println("ingrese datos concretos");
                        validar = true;
                     }
                }     
          }while(validar);
        do{
              System.out.print("El campo de vision de los agentes sera ? 0-3: ");
               campoVision=sc.nextInt();
               if (campoVision > 3){
                    System.out.println("Ingrese valore menore a 3, Intente de nuevo");
                    validar = true;
               }else
                    validar = false;
        }while(validar);
        do{
                System.out.print("El numero de dias que tendra el universo sera: ");
                dias=sc.nextInt();
                if(dias <= 0){
                    System.out.println("Ingrese valores positivos y diferente de 0");
                    validar = true;
                }else
                     validar = false;
        }while(validar);
        do{
            System.out.print("La legitimitad de los agentes al gobieron  sera? 0-100%): ");
            legitimidadGob= (sc.nextInt())/100.0;
            if(legitimidadGob >100){
                System.out.println("Ingrese valores menores o iguales a 100, intente de nuevo");
                validar = true;
            }else{
                validar = false;
            }
        }while (validar);
        do{
            System.out.print("Desea que haya movimiento?  ");
            movimiento = sc.next();
            movimiento = movimiento.toUpperCase();
            if(movimiento.equals("SI") || movimiento.equals("NO")){
                validar = false;
                
            }else{
                System.out.println("Ingrese si o no, para continuar");
                validar = true;
            }
        }while (validar);
        Boolean mov = movimiento.equals("SI");
        Universo world = new Universo(mov, dias, dimension);
       world.crearUniverso(dimension, densidadPoli, densidadCiudad,legitimidadGob);
       world.ActualizarUniverso(legitimidadGob);
       world.crearArchivoParametros();
//       world.crearArchivoSimulacion(dias);
       //world.imrpimirParametros(legitimidadGob);
        
    }
 public static void main(String[] args) throws InterruptedException, IOException {
       // TODO code application logic here
      presentacion();
     
   }

    public static int getCampoVision() {
        return campoVision;
    }
    public static int getDimension() {
        return dimension;
    }

    public static int getDias() {
        return dias;
    }

    public static int getDensidadCiudad() {
        return densidadCiudad;
    }

    public static int getDensidadPoli() {
        return densidadPoli;
    }

    public static double getLegitimidadGob() {
        return legitimidadGob;
    }

    public static void setDimension(int dimension) {
        BehaviorSocial.dimension = dimension;
    }

    public static void setDias(int dias) {
        BehaviorSocial.dias = dias;
    }

    public static void setDensidadCiudad(int densidadCiudad) {
        BehaviorSocial.densidadCiudad = densidadCiudad;
    }

    public static void setDensidadPoli(int densidadPoli) {
        BehaviorSocial.densidadPoli = densidadPoli;
    }

    public static void setCampoVision(int campoVision) {
        BehaviorSocial.campoVision = campoVision;
    }

    public static void setLegitimidadGob(double legitimidadGob) {
        BehaviorSocial.legitimidadGob = legitimidadGob;
    }
    
}
