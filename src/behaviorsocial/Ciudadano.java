/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviorsocial;
import java.util.*;
/**
 *
 * @author Gilson Ponce Briones
 */
public class Ciudadano extends Agente{
    Random rd= new Random();
    private boolean encarcelado= false;
    private double perjuicioRecibido;
    private boolean rebelado=false;
    private int turnosCarcel;
    private double aversionRiesgo;
    final double k=2.3;
    private final double limite = 0.1;
    final int MAXCAREL= 2;
   
    public Ciudadano(Ubicacion lugar){
        super(lugar);
        this.encarcelado= false;
        this.aversionRiesgo= (rd.nextInt(100)/100.0);
        this.perjuicioRecibido=(rd.nextInt(100)/100.0);
        this.rebelado=false;
        this.turnosCarcel=0;
        
    }
    //getter
    public boolean getRebelado(){
        return rebelado;
    }
    public double  calcularAgravio(double legitimidadGobierno){
        //System.out.println("Perjuicio Recibido: "+perjuicioRecibido);
        double agravio=perjuicioRecibido*(1.0-(legitimidadGobierno)/100.0);
        return agravio;
    }
    public double probabilidadRetencion(double C, double A){
        double probabilidad = 1.0-Math.exp(-k*Math.round(C/A));
        
        return probabilidad;
    }
    public double calcularRiesgoneto(double C, double A){
        //System.out.println("Aversion riesgo: "+aversionRiesgo);
        double riesgo = aversionRiesgo * probabilidadRetencion(C,A);
       
        return riesgo;
    }
   public void seRebela(double legitimidadGobierno,double C, double A){
       double valor = calcularAgravio(legitimidadGobierno) - calcularRiesgoneto(C,A);
       //System.out.println(valor);
       if (valor > limite){
           this.rebelado = true;
       }else{
           this.rebelado = false;
       }
   }

    public boolean isEncarcelado() {
        return encarcelado;
    }

    public void setEncarcelado(boolean encarcelado) {
        if (!isEncarcelado()){
            this.rebelado = false;
            this.encarcelado = encarcelado;
        }        
    }

    public boolean isRebelado() {
        return rebelado;
    }

    public void setRebelado(boolean rebelado) {
        this.rebelado = rebelado;
    }

    public Ubicacion getLugar() {
        return lugar;
    }

    public void setLugar(Ubicacion lugar) {
        this.lugar = lugar;
    }

    public int getTurnosCarcel() {
        return turnosCarcel;
    }

    public void setTurnosCarcel(int turnosCarcel) {
        this.turnosCarcel = turnosCarcel;
    }
    
    public void liberar(){
        this.turnosCarcel=0;
        this.rebelado=false;
        this.encarcelado=false;
    }

    public String toString(){
        if (isRebelado()){
            return "R ";
        }else if (isEncarcelado()){
            return "0 ";
        }else{
            return "C ";
        }
    }
    
   
}
