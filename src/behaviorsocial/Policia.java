/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviorsocial;
import java.util.*;
/**
 *
 * @author Gilson
 */
public class Policia extends Agente{
    private int rango;
    public Policia(Ubicacion lugar){
        super(lugar);
        this.rango= BehaviorSocial.getCampoVision();
        
    }
    public void mover(){
        System.out.println("Patrullando!   ");
    }
    public void  encarcelar(ArrayList<Agente> rebelados, Ubicacion uPolicia, int r, Agente[][] uni){
            int fi1 = uPolicia.getFila();
            int col1 = uPolicia.getColumna();
            System.out.println(uPolicia);
            for(Agente c: rebelados){
                Ubicacion ubi = c.getLugar();
                
                int ciufila = ubi.getFila();
                int ciucolum = ubi.getColumna();
                if (fi1 >= ciufila && ciufila <= 1+r && col1 >= ciucolum && col1 <= ciucolum+r){
                    c.setLugar(null);
                    uni[ciufila][ciucolum] = null;
                }
            }
   }
    public String toString(){
        return "P ";
    }
    /*public void patrullar(){
        System.out.println("Patrullando : bip boop ");
        ArrayList<Agente> vecindario= new ArrayList<>();
       //tengo que ver como llenar la matriz vecindario
        for(Agente a :vecindario){
            if(a instanceof Ciudadano){
                ((Ciudadano) a).seRebela(BehaviorSocial.getLegitimidadGob(), );
                if(((Ciudadano) a).isRebelado()== true){
                    Universo.getRebelados().add((Ciudadano)a);
                }
                
            }
        }
    }*/

    public int getRango() {
        return rango;
    }

    public Ubicacion getLugar() {
        return lugar;
    }
    
}
